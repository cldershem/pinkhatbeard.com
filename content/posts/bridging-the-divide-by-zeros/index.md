+++
title = "Bridging the Divide by Zeros"
template = "post.html"
date = 2024-10-30

[taxonomies]
tags = ["ai", "indyhackers", "panel", "communication"]
+++
![image of Bridging Divide Poster](bridging-divide.png)

Yesterday I had the opportunity to attend a panel discussion by Indy Hackers & Elevate Ventures. Indy Hackers is the local umbrella group for all of the other nerd groups[^1]. Elevate Ventures is a local pre-seed/early stage VC (venture capital) fund with a bit of perceived clout in the area.

The title, "Bridging the Business Technical Divide", is a topic I think about quite often. How do you get technical people and non-technical people to understand each other and to be on the same page? I really think that this is the catalyst for a significant number of failures, delays, and miscommunication in the tech world. I also think it's not something that is easily taught. In fact, another attendee/colleague of mine agreed that we both think it's kind of our Super Power. Maybe I took more offense to the discussion because it's essentially how I make a living, but I really don't think this panel was a constructive as they'd hoped.

First, let's just get something out of the way. If you're going to have a meetup, meeting, lecture, panel, discussion anything where there is more than 10 feet between you and your audience, you must use amplification. I don't care that you don't like the sound of your voice and or that it's annoying to pass a microphone. Just do it. Hire a real A/V company if it's too much of a hassle. This is for accessibility, ~8% of Marion County is Deaf or Hard of Hearing[^2]. Not to mention that not every panel member has the voice to fill a room. Strangely, it's always the able bodied, white guy that says, "we don't need this mic, do we? you can hear me, right?" -- completely ignoring that if they didn't say that into the microphone the d/Deaf crowd wouldn't be able to hear to know to respond! What about the one speaker who speaks at a lower volume, are you just planning on skipping them now? I'm not asking for an interpreter, just to even the playing field for your speakers and your HoH guests. We'll leave the inaccessible seating for another day. 

---
Speaking of privilege, when did nerds becomes the experts on "blue-collar"? Again, let's just ignore how condescending it sounds when you attempt to label groups as such, we'll get nowhere fast. A question from the audience was asked regarding if AI will make Software Engineering a blue-collar job. By the very definitions straight from Wikipedia this is a silly notion: 

> "A blue-collar worker is a person who performs manual labor or skilled trades. Blue-collar work may involve skilled or unskilled labor."[^3]

> "In contrast, the white-collar worker typically performs work in an office environment and may involve sitting at a computer or desk."[^3]

For brevity[^4], let's try to find a more simple definition of blue vs white collar.

Maybe it's college vs no college? Maybe there is a difference between learning from a book in college and learning on the job by way of an internship or apprenticeship? According to the 2024 Stack Overflow Survey[^5], only 66% of that room has some kind of degree. That can't be it.

Is it skilled vs unskilled labor? Let's say typing isn't a skill, but knowing what programming language goes with which compiler definitely isn't an innate knowledge each of us is born with, is it?

All that is to say, dude, even if your job is replaced by AI, no one is gonna take your polo shirt and office chair away and hand you some coveralls and steel toed boots. Turns out your skills aren't transferable that way, you'd probably be pretty useless on a job-site too. And even if that does make us blue-collar....who cares? Is there anything wrong with that? Personally, I don't care for collars at all.

Okay, let's move on from how ignorant our world can be of our privilege for a moment and get back to the answer. Of the 5 members of the panel, 4 of them said that if their shareholders wanted AI they'd figure out how to include AI. Mind you, none of these companies are publicly traded and so they're not beholden to any obligations to their shareholders. Even if they were, how is adding AI for the sake of AI in the best interest of anyone? Only one member of the panel even bothered to mention, "see if AI makes any business sense". To me this sounded like none of the panel had found the right technical person to explain how AI works to them or, more importantly, the right non-technical person to explain how to determine when and what features to a product.

---
"Product is the Problem" was a chant repeated, in jest, throughout by one of the panelist. Every time they mentioned it they made sure to make it clear they were "having a laugh", but it was clear that the majority of the panel believes that. There was an obvious air of US vs THEM. Maybe that explains why no one pointed out the absurd idea that suddenly AI would take our engineering jobs away and put us all in the ditches. They honestly believe that there is some kind of caste system in software. This reminded me so much of when I worked in the film industry with their concept of "above the line" and "below the line". However, at least that made sense for the most part since it relates to an actual physical line and pay rates[^6]. Though, I rarely remember anyone being as condescending towards their crew since everyone understood you wouldn't have a job if it wasn't for the people making the product.

---
Towards the end there was a question from the audience that can be paraphrased as:

> "What do you do about the one doom and gloom guy in your office that keeps saying, 'If we don't do or fix $XYZ we'll be dead in two years!'"

Of the 5 panelists multiple suggested that maybe that guy doesn't "fit in"[^7]. The only technical person on the panel suggested distracting him with another problem, "Engineers need problems to solve. If you're they're not solving yours, they'll come up with some on their own." They're not wrong, that can be true, however, it's not always the case.

It was suggested more than once that maybe the engineer was missing context. "They might think the whole system will shut down if we get 2000x growth. We need to sit them down and show them we've only ever had 3x grown on our best years so we don't need it to 'scale' like that." I do agree that it's possible that there is missing context, but not a single one of the panelists mentioned, "wait....maybe we should hear the guy out" (One panelist did say the word 'listen', but they were cut off so we can't be sure where they were going with it).

I have been at that company. I have been that guy. I have actually witnessed CEO's let their companies burn down instead of listening to that engineer who was correct. It doesn't really matter how the situation happened, but stuff happens sometimes. It might not have been on their watch, maybe it was and maybe it was pointed out at the time...if a company refuses to listen now that it's about to go up in smoke, they're probably one that probably rolled their eyes at all of the previous warnings too. What if the problem isn't a scaling issue? What if it's a security issue? One where someone has noticed that every single password may be vulnerable or that Little Bobby Droptables[^8] has an open invitation to your app. 

---
Ultimately this can all be summed up by one of the earlier discussions. There was a question of how to get the engineer and the customer on the same page. The answers were mostly about how to hide those pesky neckbeards away and to teach them empathy. At no point did anyone stop and ask, wait, why didn't our Project Manager or Product Designer catch these problems earlier? Why were the requirements missing? Does our Customer Service not have the tools they need to document this need well enough?

Maybe all of those things were done well and there is still a reason for your engineer to be on a call...maybe there should be less fear of the words your engineer will speak. If you treat them like humans, maybe they'll soon be able to empathize with humans!

---
Am I overreacting? Probably. Anyone who knows me knows that I have a penchant for hyperbole (I have an art degree, sue me). Was the panel full of evil humans? No. Did they have bad intentions? No.

Do I have the solution? Maybe.

If you're having problems communicating with a group of people, first ask if they're the problem or if you are. Start with your prejudices and realize that users, customers, and engineers are all humans and you absolutely need them all. Your business doesn't exist if not for other humans. Start there, learn to listen, and maybe then you'll have learned enough context to speak (hopefully using a microphone so everyone can hear).

If you're still having problems or want to outsource the solution, [I'm available](@/pages/work.md) and would love to [meet to discuss](https://pinkhatbeard.com/meet) your specific needs.

---
---
[^1]: Well it's supposed to be. Really, they throw a holiday social once a year, give out some awards, raise some money for Second Helpings, and disappear into the ether again for another year. Supposedly this year they're different...I hope it's true.

[^2]: https://www.in.gov/fssa/ddrs/files/DHHSCensus.pdf 

[^3]: https://en.wikipedia.org/wiki/Blue-collar_worker

[^4]: Yes, this is me being brief, if you'd like to hear a full talk on this, feel free to [book me](https://pinkhatbeard.com/meet).

[^5]: https://survey.stackoverflow.co/2024/developer-profile#1-educational-attainment

[^6]: https://www.backstage.com/magazine/article/above-the-line-vs-below-the-line-crew-differences-74969/

[^7]: which is tech speak for "fire the dude for 'cultural reasons'" -- Tech is the only industry where you can say that and no one reads it as inherently patronizing and borderline bigotry

[^8]: https://xkcd.com/327/
