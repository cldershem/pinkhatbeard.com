+++
title = "rates"
template = "page.html"
path = "rates"
+++

## Offerings

Every project is different, but I've found that a fair amount of the calls I get fall into a few categories. I've tried to make a few "packages" that set everyone's expectations clearly from the start. Clients get a known cost and fixed timeline along with a valuable deliverable. Because you're buying blocks of hours in advance and we've limited scope, I'm able to give a discount rate. On top of the fixed rate and finite Statement of Work, this allows us to work together and figure out if this is a relationship we want to maintain while still giving a highly valuable report. My goal is to make each client happy at each stage so that they invest in me for the next stage. However, if we decide it's not a good fit, no hard feelings, you've got a solid document you can take to any other consulting firm in town and use as a foundation for continuing the work.

### Tech Review & Audit

This package is an outside assessment of your companies status quo, a surface level audit. Is your team following best practices? Do you have any glaring security deficiencies? Do you have disaster recovery? After a short 2-3 week engagement, you'll receive a report with the current health and risk factors of your current tech along with recommendations on how to move forward with this new information.

{% details(summary="
#### Report Outline
") %}
- SDLC - is the team doing best practices and standards?
  - version control
  - containers
  - DevOps/Automation
  - logging
  - documentation
  - change management process review
  - time to fix bug assessment
- Disaster Recovery
  - do you have backups?
  - do they work?
  - is the process automated?
- Code Quality
  - verify test suite
  - licensing and IP compliance
  - is the code maintainable
  - is the architecture scalable?
  - is the code up to date
  - 3rd party audit
- Security Audit
  - Outstanding CVEs
  - OWASP top 10
  - retention policy review
  - hardware and device firmware review
- Current Status
- Risk Assessment
- Recommendations

#### Example Timeline
- Day 1: Outline needs
- Day 2: Sign contracts
- Day 3 - 20: Assess
- Day 21 - 30: Write report
- Day 31: Review report with stakeholders
{% end %}

### Discovery & Product Design
Now that you know where you stand, do you know where you're going? Let's get you from zero to design or from idea to wireframes. 

{% details(summary="
#### Example Timeline
") %}
- Day 1: Outline needs
- Day 2: Sign contracts
- Day 3 - 5: Review current status. Create Business Model Canvas.
- Week 2: Gather basic requirements
- Week 3 - 4: Find any domain expertise needed
- Week 5 - 6: Whiteboard with Customers and Stake Holders
- Week 7 - 8: Transpose whiteboard drawing into wireframes
- Week 9: Present wireframes and completed requirements to Stake Holders
- Week 10: Optional test suite preparation
{% end %}

### Application Development & General Consulting
Need to hash out a problem? Unsure what your next steps are? Does your team need a customized deep dive into a new problem space?

### Rates*
| Description                | Rate**              | Estimated Timeline |
| -----------                | -----               | ---------------    | 
| Tech Review & Audit        | $12,500             | ~4 - 6 weeks       | 
| Discovery & Product Design | $25,000             | ~6 - 8 weeks       | 
| General Conulting          | $130/hr             |                    | 
|                            | $4200/wk            | ~30 hours          | 
|                            | $6,000 - $12,000/mo | ~40-120 hours      | 

*These are "best guess" rates. Every project is different. Please reach out directly to discuss specific needs.

**rates do not include materials or travel

*** Discounts available for upfront payments or allowing me to write a Case Study should the work warrant it.

**** Discounts may also be available to not argue with me about tech stacks.
