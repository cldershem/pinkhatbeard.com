+++
title = "work"
template = "page.html"
path = "work"
+++

## Overview

Hi, hello. I'm Cameron. I'm good at helping people figure out problems.

I'm very much an [axe sharpener](https://www.reddit.com/r/QuotesPorn/comments/kxtqga/give_me_six_hours_to_chop_down_a_tree_and_i_will/). If you need quick, haphazard, work done without questions - I am not who you're looking for. If you need a partner who will understand the problem space and work with you until you're comfortable with the solution - I might be it.

### General Consulting

- Team stuck?
  - I'll be the [rubber duck](https://en.wikipedia.org/wiki/Rubber_duck_debugging) you've always needed
  - I'm available for code review and pair programming on an as needed basis.
- Need a translator between Stake Holders and Tech?
  - I'm fluent in nerd, but capable of breaking down highly technical problems for non-technical people. Your Stake Holders will either understand your project or understand that they'll never understand your project, but trust that you've hired the right people.
- Unsure of what you need at all?
  - I'll listen and ask every question imaginable until you've got your idea fleshed out and are working on a [Business Model Canvas](https://en.wikipedia.org/wiki/Business_Model_Canvas).

### Fractional CTO

- Need a medium to long term partner to take ownership and give advice on your product?
- Need someone to fill in while you search for the right permanent candidate?
- Are you a tech company who doesn't know they're a tech company?
- How about a CTO matchmaker?

### Prototyping / Proof of Concept / MVP

- Already have requirements? Need to prove out a concept or fail fast? 
- Need someone to dive in, gain domain expertise, and figure out how to get started?

### Project Discovery / Foundations / Product Design

- requirements gathering
- scope determination
- risk assessment
- product roadmap

### Tech Review Audit & Assessment

- Do you have legacy code that needs to be brought up to date?
- Untrusted code that needs an outside set of eyes to audit?
- Do you know your team's [bus factor](https://en.wikipedia.org/wiki/Bus_factor)?
  - Do you have a _working_ disaster recovery plan?
  - Is your documentation accurate and up-to-date?
  - Do you have your architecture documented and diagrammed
  - Are your tests actually testing?

### [Rates](@/pages/rates.md)
