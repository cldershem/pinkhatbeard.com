+++
title = "talks"
template = "page.html"
+++
# Lightning Talks
- [Rust Belt Rust 2017 - Embedded Rust](https://www.youtube.com/watch?v=1BIb4Vmwt3k&t=24s)
- [Community Level Development - PyCon 2017](http://pyvideo.org/pycon-us-2017/lightning-talks-may-19th-2017-evening-session.html)
- [Save PyVideo.org - Pycon 2016](http://pyvideo.org/pyohio-2016/saturday-lightning-talks.html)

# Talks
- [Connecting the Physical World to the Digital World - IndyPy Sept 2017](https://www.youtube.com/watch?v=SXHJmInE1xE)
